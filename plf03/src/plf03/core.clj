(ns plf03.core)

(defn funcion-comp-1
  []
  (let [c (fn [xs] (filter boolean? xs))
        z (comp c)
        ] 
    (z (list true "uno" false)) 
  ))

(defn funcion-comp-2
  [] 
  (let [
        f (fn [xs] (filter even? xs))
        z (comp f)
        ]
  (z [2 3 4 5])
))

(defn funcion-comp-3
  []
  (let [a (fn [xs] (count xs))
        z (comp a)
        ]
    (z ["uno" "dos" "tres"])
))

(defn funcion-comp-4
  []   
  (let [a (fn [xs] (map boolean? xs))
        b (fn [xs] (map neg? xs))
        z (comp a b)]
    (z [1 -2 3 -3])
))

(defn funcion-comp-5
  []
  (let [b (fn [xy] (map neg? xy))
        z (comp b)]
    (z [1 -2 3 -3])))
        
(funcion-comp-1 )
(funcion-comp-2 )
(funcion-comp-3 )
(funcion-comp-4 )
(funcion-comp-5 )


    
